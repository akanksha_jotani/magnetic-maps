function final_output = ST_segment_parameter(Bz_TTsegment)
%% Input:
% Bz_TTsegment: The magnetic field values from Tonset  
%               6 * 6 * n (n is number of samples based on sampling frequency, From Tonset to Tonset+30ms)

%% Output:
% final_output: Returns the value based on the parameters range. 
%               0 for CAD (anyone parameter out of range) and 1 for Healthy Patient

%% Method:
% The classification of the patients based on the whether the parameters
% lie in the range defined for the healthy patient.There are three
% parameters that are calculated for which seperate functions are created:
% 1. Angle varations during the segment
% 2. Distance variations during the segment
% 3. Ratio variations during the segment
% If any one parameter value lies outside range defined, patient is
% classified as CAD patient.
%% Process Algorithm
% 1. Angle Variation during the TT Segment: output_angle is 0 for CAD Patient  and 1 for Healthy Patient

[angle,devation_angle,output_angle]= angle_variation_stsegment(Bz_TTsegment); %#ok<*ASGLU>

% 2. Distance Variations during the TT Segment: output_distance is 0 for CAD Patient  and 1 for Healthy Patient

distance_ratio =1; %%% in mm  (depends on the distance between two sensor)

[distance, devation_distance,output_distance] = distance_variation_stsegment(Bz_TTsegment, distance_ratio);

% 3. Ratio Variations during the TT Segment: output_ratio is 0 for CAD Patient  and 1 for Healthy Patient
[ratio, devation_ratio,output_ratio] = ratio_variation_stsegment(Bz_TTsegment);

% Final Output : AND of the three parameters : All Parameters in range => Healthy Patient Else CAD Patient
final_output = output_angle && output_distance && output_ratio;

end

function [ratio, devation_ratio,output_ratio] = ratio_variation_stsegment(Bz_TTsegment)

%% Input:
% Bz_TTsegment: The magnetic field values from Tonset 
%               6 * 6 * n (n is number of samples based on sampling frequency, From Tonset to Tonset+30ms)

%% Outputs:
% ratio:           Values of the Ratio of Positive to Negative Peak Magnetic Field Value during the Segment
% deviation_ratio: Devation in the Ratio Values during the Segment
% output_ratio:    Outputs 0 for CAD patient and 1 for Healthy Patient based on the range values

%% Method:
% The Ratio Variation calculates the ratio of magnetic field value at
% positive peak to negative peak. This value should not vary more than 0.3
% during the 30 ms from the Tonset. This ratio variation more than 0.3 can
% result with disease patient. Hence the patient with variation of ratio
% less than 0.3 during the 30 ms range is said to be HEALTHY PATIENT and
% the patient with variation of ratio more than 0.3 during 30 ms after
% Tonset is said to be CAD PATIENT.

%% Process Algorithm:
ratio = zeros(size(Bz_TTsegment,3),1);
devation_ratio = zeros(size(Bz_TTsegment,3),1);

for i=1: size(Bz_TTsegment,3)
    Bz= Bz_TTsegment(:,:,i);
    [positive_peak,negative_peak] = peak_determination(Bz);
    positive_peak=round(positive_peak);
    negative_peak=round(negative_peak);
    positive_Bz= Bz(positive_peak(1,1), positive_peak(1,2));
    negative_Bz= -1* Bz(negative_peak(1,1), negative_peak(1,2));
    ratio(i,1)= positive_Bz/negative_Bz;
end
minimum_ratio = min(ratio);                        %% Minimum value of the ratio is calculated to check the deviations 

for i=1:size(Bz_TTsegment,3)
    devation_ratio(i,1)=ratio(i,1)-minimum_ratio;  %% Deviations in ratio of magnetic field for the time period of ST segment
end
if(devation_ratio(:,1)>0.3)                        %% Classification of the patient based on the ratio variations
    display('Variation of the ratio of magnetic field strength is more than 0.3');
    output_ratio = 0;                              %%% Patient is classified as CAD patient
else
    output_ratio = 1;                              %%% Patient is classified as Non CAD patient
end




end

function [distance, devation_distance, output_distance]= distance_variation_stsegment(Bz_TTsegment,distance_ratio)

%% Input:
% Bz_TTsegment: The magnetic field values from Tonset 
%               6 * 6 * n (n is number of samples based on sampling frequency, From Tonset to Tonset+30ms)
% distance_ratio: The distance between the sensors in mm range to convert the pixel to mm range. (Pixel size) 

%% Outputs:
% distance:           Values of the distance of Positive to Negative Peak Magnetic Field Value during the Segment
% deviation_distance: Devation in the distance Values during the Segment
% output_distance:    Outputs 0 for CAD patient and 1 for Healthy Patient based on the range values

%% Method:
% The Distance Variation calculates the variations in distance from positive 
% peak to negative peak. This value should not vary more than 20 mm during
% the 30 ms from the Tonset. This distance variation more than 20 mm can
% result with disease patient. Hence the patient with variation of distance
% less than 20mm during the 30 ms range is said to be HEALTHY PATIENT and
% the patient with variation of ratio more than 20mm during 30 ms after
% Tonset is said to be CAD PATIENT.

%% Process Algorithm:
distance = zeros(size(Bz_TTsegment,3),1);
devation_distance = zeros(size(Bz_TTsegment,3),1);

for i=1: size(Bz_TTsegment,3)
    Bz= Bz_TTsegment(:,:,i);
    [positive_peak, negative_peak] = peak_determination(Bz);
    distance(i,1)= distance_ratio *sqrt((positive_peak(1,1)-negative_peak(1,1))^2 + (positive_peak(1,2) - negative_peak(1,2))^2); 
end
minimum_distance = min(distance);                               %% Minimum Distance to Calculate the Variations

for i=1:size(Bz_TTsegment,3)
    devation_distance(i,1)=distance(i,1)-minimum_distance;      %% Deviations in distance for the time period of ST segment
end
if(devation_distance(:,1)>20)                                   %% Classification of the patient based on the distance variations
    display('Variation of the distance is more than 20 mm');
    output_distance = 0;                                        %%% Patient is classified as CAD patient
else
    output_distance = 1;                                        %%% Patient is classified as Non CAD patient
end

end

function [angle,devation_angle,output_angle]= angle_variation_stsegment(Bz_TTsegment)

%% Input:
% Bz_TTsegment: The magnetic field values from Tonset 
%               6 * 6 * n (n is number of samples based on sampling frequency, From Tonset to Tonset+30ms)

%% Outputs:
% angle:           Values of the Angle from Positive to Negative Peak of Magnetic Field during the Segment
% deviation_angle: Devation in the Angle Values during the Segment
% output_angle:    Outputs 0 for CAD patient and 1 for Healthy Patient based on the range values

%% Method:
% The Angle Variation calculates the angle of magnetic field value between
% positive peak and negative peak. This value should not vary more than 45
% during the 30 ms from the Tonset. This ratio variation more than 45 can
% result with disease patient. Hence the patient with variation of ratio
% less than 45 during the 30 ms range is said to be HEALTHY PATIENT and
% the patient with variation of ratio more than 45 during 30 ms after
% Tonset is said to be CAD PATIENT.

%% Process Algorithm
angle = zeros(size(Bz_TTsegment,3),1);
devation_angle = zeros(size(Bz_TTsegment,3),1);

for i=1: size(Bz_TTsegment,3)
    Bz= Bz_TTsegment(:,:,i);
    [positive_peak, negative_peak] = peak_determination(Bz);
    dx= positive_peak(1,1)-negative_peak(1,1);
    dy= positive_peak(1,2)-negative_peak(1,2);
    a1(i,1)=atan(dy,dx);                          %% Angle Calculation for the time period of ST segment
    angle(i,1)=(a1(i,1)*180)/pi;
end
minimum_angle= min(angle);
for i=1:size(Bz_TTsegment,3)
    devation_angle(i,1)=angle(i,1)-minimum_angle;     %% Deviations in Angle for the time period of ST segment
end
if(devation_angle(:,1)>45)                                      %% Classification of the patient based on the angle variations
    display('Variation of the angle more than 45 degree');
    output_angle =0;                              %%% Patient is classified as CAD patient
else
    output_angle =1;                              %%% Patient is classified as Non CAD patient
end
    

end
function [positive_peak, negative_peak] = peak_determination(Bz)
%%% Bz = 6 * 6 matrix
%% Inputs:
% Bz : Magnetic Field Matrix of 6x6 array

%% Outputs :
% positive_peak: Location of the positive peak in the Magnetic Maps 
% negative_peak: Location of the negative peak in the Magnetic Maps


%% Method:
% This methods combines all the positive values of the magnetic field to
% find the final location of the positive peak. Similarly all the negative
% values of the magnetic field are combined to calculate the negative peak
% location.
% 1. Collecting all positive values for Positive Peak Location
% 2. Collecting all negative values for Negative Peak Location

%% 1. Finding the location of the Bz_positive(x,y) pole
[r_p,c_p]=find(Bz>0);                               % Find the location of all positive magnetic field from array
position_positive = [c_p r_p];
Bz_positive_values= zeros(size(position_positive,1),1);
mxp=zeros(size(r_p,1),1);
myp=zeros(size(r_p,1),1);

for i=1:size(position_positive, 1)
    Bz_positive_values(i,1)=Bz(position_positive(i,2), position_positive(i,1));
end
for i=1:size(r_p,1)
    mxp(i,1) = Bz_positive_values(i,1)*c_p(i,1);
    myp(i,1) = Bz_positive_values(i,1)*r_p(i,1);
end
xpositive = sum(mxp)/sum(Bz_positive_values);       % Column Value of the positive peak of magnetic field
ypositive = sum(myp)/sum(Bz_positive_values);       % Row Value of the positive peak of magnetic field


%% 2.Finding the location of the Bz_positive(x,y) pole
[r_n,c_n]=find(Bz<0);                                % Find the location of all negative magnetic field from array
position_negative = [c_n r_n];
Bz_negative_values= zeros(size(position_negative,1),1);
mxn=zeros(size(r_n,1),1);
myn=zeros(size(r_n,1),1);

for i=1:size(position_negative, 1)
    Bz_negative_values(i,1)=Bz(position_negative(i,2), position_negative(i,1));
end
for i=1:size(r_n,1)
    mxn(i,1) = Bz_negative_values(i,1)*c_n(i,1);
    myn(i,1) = Bz_negative_values(i,1)*r_n(i,1);
end
xnegative = sum(mxn)/sum(Bzn);
ynegative = sum(myn)/sum(Bzn);

%% 3. Final Locations of Positive and Negative Peak

positive_peak = [xpositive ypositive];    % Positive Peak Location
negative_peak = [xnegative ynegative];    % Negative Peak Location

end
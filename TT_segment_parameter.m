function final_output = TT_segment_parameter(Bz_TTsegment,distance_ratio)
%% Input:
% Bz_TTsegment: The magnetic field values from Tonset to Toffset
%               6 * 6 * n (n is number of samples based on sampling frequency, From Tonset to Toffset)
% distance_ratio: The distance between the sensors in mm range to convert the pixel to mm range. (Pixel size)

%% Output:
% final_output: Returns the value based on the parameters range. 
%               0 for CAD (anyone parameter out of range) and 1 for Healthy Patient

%% Method:
% The classification of the patients based on the whether the parameters
% lie in the range defined for the healthy patient.There are four
% parameters that are calculated for which seperate functions are created:
% 1. Maximum Angle variations during the segment
% 2. Minimum Angle variations during the segment
% 3. Angle varations during the segment
% 4. Distance variations during the segment
% If any one parameter value lies outside range defined, patient is classified as CAD patient.

%%
% 1. Maximum Angle Variation during the TT Segment: output_angle is 0 for CAD Patient  and 1 for Healthy Patient
[angle_maximum,output_angle_maximum]= angle_maximum_ttsegment(Bz_TTsegment); 

% 2. Minimum Angle Variation during the TT Segment: output_angle is 0 for CAD Patient  and 1 for Healthy Patient
[angle_minimum,output_angle_minimum]= angle_minimum_ttsegment(Bz_TTsegment); %#ok<*ASGLU>

% 3. Angle Variation during the TT Segment: output_angle is 0 for CAD Patient  and 1 for Healthy Patient
[angle,devation_angle,output_angle]= angle_varation_ttsegment(Bz_TTsegment);

% 4. Distance Variations during the TT Segment: output_distance is 0 for CAD Patient  and 1 for Healthy Patient
%%% in mm  (pixel size/ distance between two sensor)
[distance, devation_distance,output_distance] = distance_variation_ttsegment(Bz_TTsegment,distance_ratio);

% Final Output : AND of the three parameters : All Parameters in range => Healthy Patient Else CAD Patient
final_output = output_angle_maximum && output_angle_minimum && output_distance && output_angle;

end

function [distance, devation_distance, output_distance]= distance_variation_ttsegment(Bz_TTsegment,distance_ratio)

%% Input:
% Bz_TTsegment: The magnetic field values from Tonset to Toffset
%               6 * 6 * n (n is number of samples based on sampling frequency, From Tonset to Toffset)
% distance_ratio: The distance between the sensors in mm range to convert the pixel to mm range. (Pixel size) 

%% Outputs:
% distance:           Values of the distance of Positive to Negative Peak Magnetic Field Value during the Segment
% deviation_distance: Devation in the distance Values during the Segment
% output_distance:    Outputs 0 for CAD patient and 1 for Healthy Patient based on the range values

%% Method:
% The Distance Variation calculates the variations in distance from positive 
% peak to negative peak. This value should not vary more than 26 mm during
% the TT interval. This distance variation more than 26 mm can result with
% disease patient. Hence the patient with variation of distance
% less than 26 mm during the TT interval is said to be HEALTHY PATIENT and
% the patient with variation of ratio more than 26mm during TT Interval is
% said to be CAD PATIENT.

%% Process Algorithm:
distance = zeros(size(Bz_TTsegment,3),1);
devation_distance = zeros(size(Bz_TTsegment,3),1);

for i=1: size(Bz_TTsegment,3)
    Bz= Bz_TTsegment(:,:,i);
    [positive_peak, negative_peak] = peak_determination(Bz);
    distance(i,1)= distance_ratio *sqrt((positive_peak(1,1)-negative_peak(1,1))^2 + (positive_peak(1,2) - negative_peak(1,2))^2); 
end
minimum_distance = min(distance);

for i=1:size(Bz_TTsegment,3)
    devation_distance(i,1)=distance(i,1)-minimum_distance;     %% Deviations in distance for the time period of ST segment
end
if(devation_distance(:,1)>26)                                      %% Classification of the patient based on the distance variations
    display('Variation of the distance is outside the range');
    output_distance = 0;                              %%% Patient is classified as CAD patient
else
    output_distance = 1;                              %%% Patient is classified as Non CAD patient
end

end

function [angle,devation_angle,output_angle]= angle_varation_ttsegment(Bz_TTsegment)
%% Input:
% Bz_TTsegment: The magnetic field values from Tonset to Toffset (TT Interval) 
%               6 * 6 * n (n is number of samples based on sampling frequency, TT Interval)

%% Outputs:
% angle:           Values of the Angle between Positive to Negative Peak of Magnetic Field during the Segment
% deviation_angle: Devation in the Angle Values during the Segment
% output_angle:    Outputs 0 for CAD patient and 1 for Healthy Patient based on the range values

%% Method:
% The Angle Variation calculates the angle of magnetic field value between
% positive peak and negative peak. This value should not vary more than 18
% during the TT Interval. This ratio variation more than 18 can
% result with disease patient. Hence the patient with variation of ratio
% less than 18 during the TT Interval is said to be HEALTHY PATIENT and
% the patient with variation of ratio more than 18 during TT Interval is
% said to be CAD PATIENT.

%% Process Algorithm
angle = zeros(size(Bz_TTsegment,3),1);
devation_angle = zeros(size(Bz_TTsegment,3),1);

for i=1: size(Bz_TTsegment,3)
    Bz= Bz_TTsegment(:,:,i);
    [positive_peak, negative_peak] = peak_determination(Bz);
    dx= positive_peak(1,1)-negative_peak(1,1);
    dy= positive_peak(1,2)-negative_peak(1,2);
    a1(i,1)=atan(dy,dx);                          %% Angle Calculation for the time period of ST segment
    angle(i,1)=(a1(i,1)*180)/pi;
end
minimum_angle= min(angle);
for i=1:size(Bz_TTsegment,3)
    devation_angle(i,1)=angle(i,1)-minimum_angle;     %% Deviations in Angle for the time period of ST segment
end
if(devation_angle(:,1)>18 && devation_angle(:,1)<0)                                      %% Classification of the patient based on the angle variations
    display('Variation of the angle outside the range');
    output_angle =0;                              %%% Patient is classified as CAD patient
else
    output_angle =1;                              %%% Patient is classified as Non CAD patient
end
    

end

function [angle,output_angle_minimum]= angle_minimum_ttsegment(Bz_TTsegment)
%% Input:
% Bz_TTsegment: The magnetic field values from Tonset to Toffset (TT Interval) 
%               6 * 6 * n (n is number of samples based on sampling frequency, TT Interval)

%% Outputs:
% angle:           Values of the Angle between Positive to Negative Peak of Magnetic Field during the Segment
% output_angle_minimum:    Outputs 0 for CAD patient and 1 for Healthy Patient based on the range values

%% Method:
% The Angle Mimimum calculates the angle of magnetic field value between
% positive peak and negative peak. Then during the TT interval it finds the
% minimum value of the angle.This value should be between -85 to -50 degree 
% during the TT Interval. This minimum angle variation more than defined 
% range can result with disease patient. Hence the patient with variation 
% of minimum angle in defined range during the TT Interval is said to be 
% HEALTHY PATIENT and the patient with variation of minimum angle more than 
% defined range during TT Interval is said to be CAD PATIENT.

%% Process Algorithm
angle = zeros(size(Bz_TTsegment,3),1);

for i=1: size(Bz_TTsegment,3)
    Bz= Bz_TTsegment(:,:,i);
    [positive_peak, negative_peak] = peak_determination(Bz);
    dx= positive_peak(1,1)-negative_peak(1,1);
    dy= positive_peak(1,2)-negative_peak(1,2);
    a1(i,1)=atan(dy,dx);                          %% Angle Calculation for the time period of ST segment
    angle(i,1)=(a1(i,1)*180)/pi;
end
minimum_angle= min(angle);

if(minimum_angle>-85 && minimum_angle<-50)      %% Classification of the patient based on the angle variations
    display('Minimum Angle during the TT interval is between range');
    output_angle_minimum =1;                              %%% Patient is classified as Non CAD patient
else
    display('Minimum Angle during the TT interval is outside the range');
    output_angle_minimum =0;                              %%% Patient is classified as CAD patient
end
    

end

function [angle,output_angle_maximum]= angle_maximum_ttsegment(Bz_TTsegment)
%% Input:
% Bz_TTsegment: The magnetic field values from Tonset to Toffset (TT Interval) 
%               6 * 6 * n (n is number of samples based on sampling frequency, TT Interval)

%% Outputs:
% angle:           Values of the Angle between Positive to Negative Peak of Magnetic Field during the Segment
% output_angle_maximum:    Outputs 0 for CAD patient and 1 for Healthy Patient based on the range values

%% Method:
% The Angle Maximum calculates the angle of magnetic field value between
% positive peak and negative peak. Then during the TT interval it finds the
% maximum value of the angle. This value should be between -98 to -33 degree
% during the TT Interval.This maximum angle variation more than this range 
% can result with disease patient. Hence the patient with variation of 
% maximum angle in range during the TT interval is said to be HEALTHY 
% PATIENT and the patient with variation of maximum angle more than defined 
% range during TT Interval is said to be CAD PATIENT.

%% Process Algorithm
angle = zeros(size(Bz_TTsegment,3),1);

for i=1: size(Bz_TTsegment,3)
    Bz= Bz_TTsegment(:,:,i);
    [positive_peak, negative_peak] = peak_determination(Bz);
    dx= positive_peak(1,1)-negative_peak(1,1);
    dy= positive_peak(1,2)-negative_peak(1,2);
    a1(i,1)=atan(dy,dx);                          %% Angle Calculation for the time period of ST segment
    angle(i,1)=(a1(i,1)*180)/pi;
end
maximum_angle= max(angle);

if(maximum_angle>-98 && maximum_angle<-33)                                      %% Classification of the patient based on the angle variations
    display('Maximum Angle during the TT interval is between range');
    output_angle_maximum =1;                              %%% Patient is classified as Non CAD patient
else
    display('Maximum Angle during the TT interval is outside the range');
    output_angle_maximum =0;                              %%% Patient is classified as CAD patient
end
    

end
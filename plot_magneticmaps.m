function []= plot_magneticmaps(Bz, interpolation_factor)
%%%% Plots the contour map for the magnetic field matrix Bz

%% Inputs: 
% Bz : Magnetic Field Matrix of 6x6 array
% interpolation_factor : factor by which the data is interpolated

%% Outputs:
% Contour Plot of the Magnetic Field

%%
if(interpolation_factor == 1)
    figure;
    contourf(Bz);
else
   Bz_interpolated = imresize(Bz,interpolation_factor,'cubic');
   figure;
   contourf(Bz_interpolated);
end

end
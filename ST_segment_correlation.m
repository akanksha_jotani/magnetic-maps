function [final_output, Correlation_value]= ST_segment_correlation(Bz_STsegment)

%% Input:
% Bz_STsegment: The magnetic field values from Jpoint to Toffset  
%               6 * 6 * n (n is number of samples based on sampling frequency, From Jpoint to Toffset)

%% Output:
% final_output: Returns the value based on the parameters range. 
%               0 for CAD (anyone parameter out of range) and 1 for Healthy Patient
% Correlation_value: Mean correlation value of the magnetic field maps during the ST-T interval.

%% Method:
% The classification of the patients based on the whether the magnetic maps
% remains same throughout the repolarization event of patients.Based on the
% fact, during repolarization the EMF vector is oriented on left to
% downwards from the start to the end of the repolarization interval. This
% suggest that the maps and magnetic field amplitude remains same during
% the repolarization and can be used for classification of patients.If
% there is an irregularity in the maps, it can be calculated using the
% correlation and this correlation value can be used to classify patients
% with CAD and healthy patient.
% Irregularity in ST-T maps => Lower Correlation Value => CAD
% Regularity in ST-T maps => Higher Correlation Value => Healthy/ Normal

%% Process Algorithm
correlation=zeros(size(Bz_STsegment,3),size(Bz_STsegment,3));

for j = 1:size(Bz_STsegment,3)
    Bz=Bz_STsegment(:,:,j);
    for i = 1:size(Bz_STsegment,3)
        if(i==j)
            correlation(j,i)=0;                             %%% Correlating same data == Neglecting this effect by assigning zero.
        else
            correlation(j,i)= corr2(Bz,Bz_STsegment(:,:,i));
        end
    end
end

% Calculating the Mean Correlation value of the maps during the ST segment(repolarization) for a patient

Correlation_value=mean2(correlation);       % Lower Value of Correlation => Irregularity 
                                            % High Value of Correlation => Regularity

%%%% Based on the clinical data, the threshold for the classification of the CAD and healthy patient
%%%% Set the threshold_for_classification value to identify the patients between diseased and Healthy patient

threshold_for_classification = 0.9;             %% SET THIS VALUE

if(Correlation_value < threshold_for_classification)
    final_output = 1;
    display('Based on Correlation of the Maps, the Patient is classified as CAD ');
else
    display('Based on Correlation of the Maps, the Patient is classified as No CAD');
end

end
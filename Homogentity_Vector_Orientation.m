function [Final_Output, H, M] = Homogentity_Vector_Orientation(Bz_STsegment)
%% Input:
% Bz_STsegment: The magnetic field values from Jpoint to Toffset  
%               6 * 6 * n (n is number of samples based on sampling frequency, From Jpoint to Toffset)

%% Output:
% Final_Output: Returns the value based on the Threshold mentioned. 
%               0 for CAD (M or H> threshold) and 1 for Healthy Patient (M and H<threshold)
% H:            Homogeneity factor of pateint during the ST-T interval.
% M:            Magnetic Map Vector factor of pateint during the ST-T interval.
%% Method:

% We have evaluated homogeneity and magneto motive force (MMF) direction 
% disturbances on magnetic field distribution maps on an extent of 
% ST-T interval under the following simple criteria.
% 1. Amount of inhomogeneous maps (i.e. maps exhibiting additional extrema)
% in percentage ratio to the total number of maps H. This criterion has 
% been used for evaluate inhomogeneity in magnetic field distribution maps.
% 2. The amount of maps in which MMF direction differs from the normal in
% percentage to total number of maps D.This criterion allows an estimation 
% of the MMF direction disturbance. This two parameters helps to calculate
% the irregularities during the repolarization in the patients.

%% Process Algorithm:

% 1. Inhomogeneity in Magnetic Field Distribution Maps 
[H,Output_H] = Homogeneity(Bz_STsegment);

% 2. Magneto Motive Force Direction Disturbances
[M,Output_M] = Magnetic_Vector_Orientation(Bz_STsegment);

% Final Output based on the Homogeneity and MagnetoMotive Force Direction
% Disturbances, the output is either patient is CAD or Healthy.

% Both parameters within threshold range, Final_Output is 1 and Patient
% classified as Healthy. 

% But if any one parameter is outside the threshold, Final_Output is 0 and 
% patient is classified as CAD.

Final_Output = Output_H && Output_M;         
if(Final_Output == 0 )
    display('Based on Homogeneity and Magnetic Vector Orientation, the patient is classified as CAD patient');
else
    display('Based on Homogeneity and Magnetic Vector Orientation, the patient is classified as Healthy patient');
end
end

function [M, Output_M] = Magnetic_Vector_Orientation(Bz_STsegment)
%% Input:
% Bz_STsegment: The magnetic field values from Jpoint to Toffset  
%               6 * 6 * n (n is number of samples based on sampling frequency, From Jpoint to Toffset)

%% Output:
% M:        Magnetic Motive Vector Factor of pateint during the ST-T interval.
% Output_M: Returns the value based on the Threshold mentioned. 
%               0 for CAD (M> threshold) and 1 for Healthy Patient (M<threshold)

%% Method:
% The amount of maps in which MMF direction differs from the normal in
% percentage to total number of maps D. This criterion allows an estimation 
% of the MMF direction disturbance. The number of maps having the MMF
% vector outside the normal range i.e. -110 to 20 degree, is considered as
% abnormal. Thus D is a ratio of number of abnormal maps to total number of
% maps.
 
%% Process Algorithm:

% Define Size of the matrices
%    a1= angle in radians 
%    angle = angle in degree 

a1=zeros(size(Bz_STsegment,3),1);
angle=zeros(size(Bz_STsegment,3),1);


for i=1: size(Bz_STsegment,3)
    Bz= Bz_STsegment(:,:,i);
    [positive_peak, negative_peak] = peak_determination(Bz); %% Determines the peaks in the maps
    dx= positive_peak(1,1)-negative_peak(1,1);          
    dy= positive_peak(1,2)-negative_peak(1,2);
    a1(i,1)=atan(dy,dx);                                     %% Angle Calculation in radians for the time period of ST segment
    angle(i,1)=(a1(i,1)*180)/pi;                             %% Angle conversion from the radians to degree.   
end
count=0;
for i=1:size(Bz_STsegment,3)
   if(angle(i,1)< -110 && angle(i,1)>20)                    %% Count the number of maps having the magnetic maps MMF vectors outside the normal range.
       count = count + 1;
   end
end
Magnetic_vector_number_outside_range = count;
Total_number_of_maps = size(Bz_STsegment,3);
M = Magnetic_vector_number_outside_range/Total_number_of_maps; %% MMF parameter calculation


%% Based on clinical data, the threshold to classify between the CAD and healthy patient
%%%% Here suppose the threshold is set as 0.3 to classify

threshold_for_classification = 0.3;                   %% SET THIS VALUE

if(M>threshold_for_classification)
    Output_M=0;
   display('Patient is a CAD patient based on the magnetic vector orientation(M)');
else
    Output_M=1;
    display('Patient is a Healthy as the magnetic vector orientation does not vary much');
end
end

function [H,Output_H] = Homogeneity(Bz_STsegment)
%% Input:
% Bz_STsegment: The magnetic field values from Jpoint to Toffset  
%               6 * 6 * n (n is number of samples based on sampling frequency, From Jpoint to Toffset)

%% Output:
% H:        Homogeneity factor of pateint during the ST-T interval.
% Output_H: Returns the value based on the Threshold mentioned. 
%               0 for CAD (H> threshold) and 1 for Healthy Patient (H<threshold)

%% Method:
% Amount of inhomogeneous maps (i.e. maps exhibiting additional extrema)
% in percentage ratio to the total number of maps is H. This criterion has 
% been used for evaluate inhomogeneity in magnetic field distribution maps.
% The regularity of the maps is used to classify the patients with CAD and
% healthy patients. Patients with CAD has more than one extrema in the
% Magnetic maps. For a patient, the H that has greater than a
% threshold is classified as the patient as Coronary Artery Disease
% patient.

%% Process Algorithm:
count=0;
for i=1:size(Bz_STsegment,3)
    Bz= Bz_STsegment(:,:,i);
    [y, x]= find(Bz,max(max(Bz)));
    if(size(y,1)>1)
        count=count+1;                                              %% Count number of maps having positive peaks more than one.
    end
end
Number_of_maps_inhomogeneous = count;
Total_number_of_maps = size(Bz_STsegment,3);
H=Number_of_maps_inhomogeneous/Total_number_of_maps;                %% H = Number of Maps having More than one Maximum Peaks/Total number of maps.

%% Based on the clinical data, the threshold can be used to classify between the patients with CAD and without CAD.

%%% Suppose the threshold is set as 0.3 to classify


threshold_for_classification = 0.3;                      %% SET THIS VALUE

if(H>threshold_for_classification)
    Output_H = 0;
    display('Patient is a CAD patient based on the Homogeneity vector (H)');
else
    Output_H = 1;
    display('Patient is a Healthy as there exist minimum inhomogeneity');
end

end
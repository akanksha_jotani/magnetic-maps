function [hd_positive_H hd_positive_E hd_positive_I hd_positive_C class]= Trajectory_Mapping(Bz_STsegment)
%% Input:
% Bz_wholesegment/Bz_STsegment: The magnetic field values from Jpoint to Toffset  
%               6 * 6 * n (n is number of samples based on sampling frequency, From Jpoint to Toffset)

%% Output:
% hd_positive_H: Hausdorff Distance between trajectory of Patient and trajectory of Healthy Patient.
% hd_positive_E: Hausdorff Distance between trajectory of Patient and trajectory of Early CAD Patient.
% hd_positive_I: Hausdorff Distance between trajectory of Patient and trajectory of Intermediate CAD Patient.
% hd_Positive_C: Hausdorff Distance between trajectory of Patient and trajectory of Severe CAD Patient.
% class:         1= Normal, 2= Early CAD, 3= Intermediate CAD and 4= Severe CAD

%% Method:

% Classification Based on Trajectories Similarity (CBoTS) is a method based
% on the classification of the patients into 4 classes based on the
% trajectory obtained from the magnetic map maxima peaks. Connecting the
% maxima peaks of the magnetic maps during the ST-T segments and creating a
% trajectory. The trajectory of the patients with Healthy patient, Early
% CAD, Intermediate CAD and Severe CAD are all different. Distance between
% the Patient with the templates in all classes are calculated using the
% Hausdorff method. The one having smallest distance finallises that the
% patient is in that respective class. This method needs the templates of
% the trajectories of Healthy Patient, Early CAD patient, Intermediate CAD
% patient and Severe CAD patient. Based on this templates, the
% classification of the patients takes places.

%% Process Algorithm:

% 1. Define the template trajectory for all classes

healthy_patient_positive_trajectory =[];                    %% Healthy Patient Trajectory
Intermediate_CAD_patient_positive_trajectory =[];           %% Intermediate CAD Patient Trajectory
CAD_patient_positive_trajectory = [];                       %% Severe CAD Patient Trajectory
Early_CAD_patient_positive_trajectory = [];                 %% Early CAD Patient Trajectory

% 2. For the test case ---- extract the positive peaks for trajecctory mapping 
for i=1: size(Bz_STsegment,3)
    Bz= Bz_STsegment(:,:,i);
    [positive_peak, negative_peak] = peak_determination(Bz); %#ok<*NASGU>
    positive_peak_set(i,:)= positive_peak;                                                  %#ok<*AGROW>
    % negative_peak_set(i,:)= negative_peak;
end

% 3. Calculation of the Distance between the Trajectories
%% Functions:
% HausdorffDist: Hausdorff Distance
% ModHaussdorffDist: Modified Hausdorff Distance
%% Outputs: 
% hd_positive_H: Hausdorff distance and 
% D_positive_H: matrix of distances


% 3.1. Distance between the Latest Patient Trajectory and Healthy Patient Trajectory

[hd_positive_H D_positive_H] = HausdorffDist(positive_peak_set,healthy_patient_positive_trajectory);

% 3.2. Distance between the Latest Patient Trajectory and Severe CAD Patient Trajectory
[hd_positive_C D_positive_C] = HausdorffDist(positive_peak_set,CAD_patient_positive_trajectory);

% 3.3. Distance between the Latest Patient Trajectory and Intermediate CAD Patient Trajectory
[hd_positive_I D_positive_I] = HausdorffDist(positive_peak_set,Intermediate_CAD_patient_positive_trajectory);

% 3.4. Distance between the Latest Patient Trajectory and Early CAD Patient Trajectory
[hd_positive_E D_positive_E] = HausdorffDist(positive_peak_set,Early_CAD_patient_positive_trajectory);

% 4. Defining the class of patient based on the distance calculation.
%%% Class = 1 Normal Patient (minimum distance between test case trajectory and healthy trajectory)
%%% Class = 2 Early CAD Patient (minimum distance between test case trajectory and Early CAD trajectory)
%%% Class = 3 Intermediate CAD Patient (minimum distance between test case trajectory and Intermediate CAD trajectory)
%%% Class = 4 Severe CAD Patient (minimum distance between test case trajectory and Severe CAD trajectory)

hausdorffdistance = [hd_positive_H hd_positive_E hd_positive_I hd_positive_C];
minimum_distance = min(haursdorffdistance);
[y, x]=find(hausdorffdistance == minimum_distance); % [x] = varies from 1 to 4  defining class based on the location of minimum distance.
if(x == 1)
    class = 1;
    display('Normal Patient');
elseif(x == 2)
    class = 2;
    display('Early CAD Patient');
elseif(x == 3)
    class =3;
    display('Intermediate CAD Patient');
else
    class =4;
    display('Severe CAD Patient');
end
    
end

function [hd D] = HausdorffDist(P,Q,lmf,dv)
% Calculates the Hausdorff Distance between P and Q
%
% hd = HausdorffDist(P,Q)
% [hd D] = HausdorffDist(P,Q)
% [hd D] = HausdorffDist(...,lmf)
% [hd D] = HausdorffDist(...,[],'visualize')
%
% Calculates the Hausdorff Distance, hd, between two sets of points, P and
% Q (which could be two trajectories). Sets P and Q must be matrices with
% an equal number of columns (dimensions), though not necessarily an equal
% number of rows (observations).
%
% The Directional Hausdorff Distance (dhd) is defined as:
% dhd(P,Q) = max p c P [ min q c Q [ ||p-q|| ] ].
% Intuitively dhd finds the point p from the set P that is farthest from
% any point in Q and measures the distance from p to its nearest neighbor
% in Q.
% 
% The Hausdorff Distance is defined as max{dhd(P,Q),dhd(Q,P)}
%
% D is the matrix of distances where D(n,m) is the distance of the nth
% point in P from the mth point in Q.
%
% lmf: If the size of P and Q are very large, the matrix of distances
% between them, D, will be too large to store in memory. Therefore, the
% function will check your available memory and not build the D matrix if
% it will exceed your available memory and instead use a faster version of
% the code. If this occurs, D will be returned as the empty matrix. You may
% force the code to forgo the D matrix even for small P and Q by calling the
% function with the optional 3rd lmf variable set to 1. You may also force
% the function to return the D matrix by setting lmf to 0. lmf set to []
% allows the code to automatically choose which mode is appropriate.
%
% Including the 'vis' or 'visualize' option plots the input data of
% dimension 1, 2 or 3 if the small dataset algorithm is used.
%
% Performance Note: Including the lmf input increases the speed of the
% algorithm by avoiding the overhead associated with checking memory
% availability. For the lmf=0 case, this may represent a sizeable
% percentage of the entire run-time.
%
%

% %%% ZCD Oct 2009 %%%
% Edits ZCD: Added the matrix of distances as an output. Fixed bug that
%   would cause an error if one of the sets was a single point. Removed
%   excess calls to "size" and "length". - May 2010
% Edits ZCD: Allowed for comparisons of N-dimensions. - June 2010
% Edits ZCD: Added large matrix mode to avoid insufficient memory errors
%   and a user input to control this mode. - April 2012
% Edits ZCD: Using bsxfun rather than repmat in large matrix mode to
%   increase performance speeds. [update recommended by Roel H on MFX] -
%   October 2012
% Edits ZCD: Added a plotting function for visualization - October 2012
%

sP = size(P); sQ = size(Q);

if ~(sP(2)==sQ(2))
    error('Inputs P and Q must have the same number of columns')
end

if nargin > 2 && ~isempty(lmf)
    % the user has specified the large matrix flag one way or the other
    largeMat = lmf;     
    if ~(largeMat==1 || largeMat==0)
        error('3rd ''lmf'' input must be 0 or 1')
    end
else
    largeMat = 0;   % assume this is a small matrix until we check
    % If the result is too large, we will not be able to build the matrix of
    % differences, we must loop.
    if sP(1)*sQ(1) > 2e6
        % ok, the resulting matrix or P-to-Q distances will be really big, lets
        % check if our memory can handle the space we'll need
        memSpecs = memory;          % load in memory specifications
        varSpecs = whos('P','Q');   % load in variable memory specs
        sf = 10;                    % build in a saftey factor of 10 so we don't run out of memory for sure
        if prod([varSpecs.bytes]./[sP(2) sQ(2)]) > memSpecs.MaxPossibleArrayBytes/sf
            largeMat = 1;   % we have now concluded this is a large matrix situation
        end
    end
end

if largeMat
% we cannot save all distances, so loop through every point saving only
% those that are the best value so far

maxP = 0;           % initialize our max value
% loop through all points in P looking for maxes
for p = 1:sP(1)
    % calculate the minimum distance from points in P to Q
    minP = min(sum( bsxfun(@minus,P(p,:),Q).^2, 2));
    if minP>maxP
        % we've discovered a new largest minimum for P
        maxP = minP;
    end
end
% repeat for points in Q
maxQ = 0;
for q = 1:sQ(1)
    minQ = min(sum( bsxfun(@minus,Q(q,:),P).^2, 2));
    if minQ>maxQ
        maxQ = minQ;
    end
end
hd = sqrt(max([maxP maxQ]));
D = [];
    
else
% we have enough memory to build the distance matrix, so use this code
    
% obtain all possible point comparisons
iP = repmat(1:sP(1),[1,sQ(1)])';
iQ = repmat(1:sQ(1),[sP(1),1]);
combos = [iP,iQ(:)];

% get distances for each point combination
cP=P(combos(:,1),:); cQ=Q(combos(:,2),:);
dists = sqrt(sum((cP - cQ).^2,2));

% Now create a matrix of distances where D(n,m) is the distance of the nth
% point in P from the mth point in Q. The maximum distance from any point
% in Q from P will be max(D,[],1) and the maximum distance from any point
% in P from Q will be max(D,[],2);
D = reshape(dists,sP(1),[]);

% Obtain the value of the point, p, in P with the largest minimum distance
% to any point in Q.
vp = max(min(D,[],2));
% Obtain the value of the point, q, in Q with the largets minimum distance
% to any point in P.
vq = max(min(D,[],1));

hd = max(vp,vq);

end





% --- visualization ---
if nargin==4 && any(strcmpi({'v','vis','visual','visualize','visualization'},dv))
    if largeMat == 1 || sP(2)>3
        warning('MATLAB:actionNotTaken',...
            'Visualization failed because data sets were too large or data dimensionality > 3.')
        return;
    end
    % visualize the data
    figure
    subplot(1,2,1)
    hold on
    axis equal
    
    % need some data for plotting
    [mp ixp] = min(D,[],2);
    [mq ixq] = min(D,[],1);
    [mp ixpp] = max(mp);
    [mq ixqq] = max(mq);
    [m ix] = max([mq mp]);
    if ix==2
        ixhd = [ixp(ixpp) ixpp];
        xyf = [Q(ixhd(1),:); P(ixhd(2),:)];
    else
        ixhd = [ixqq ixq(ixqq)];
        xyf = [Q(ixhd(1),:); P(ixhd(2),:)];
    end
    
    % -- plot data --
    if sP(2) == 2
        h(1) = plot(P(:,1),P(:,2),'bx','markersize',10,'linewidth',3);
        h(2) = plot(Q(:,1),Q(:,2),'ro','markersize',8,'linewidth',2.5);
        % draw all minimum distances from set P
        Xp = [P(1:sP(1),1) Q(ixp,1)];
        Yp = [P(1:sP(1),2) Q(ixp,2)];
        plot(Xp',Yp','-b');
        % draw all minimum distances from set Q
        Xq = [P(ixq,1) Q(1:sQ(1),1)];
        Yq = [P(ixq,2) Q(1:sQ(1),2)];
        plot(Xq',Yq','-r');
        
        % denote the hausdorff distance
        h(3) = plot(xyf(:,1),xyf(:,2),'-ks','markersize',12,'linewidth',2);
        uistack(fliplr(h),'top')
        xlabel('Dim 1'); ylabel('Dim 2');
        title(['Hausdorff Distance = ' num2str(m)])
        legend(h,{'P','Q','Hausdorff Dist'},'location','best')
        
    elseif sP(2) == 1   
        ofst = hd/2;    % plotting offset
        h(1) = plot(P(:,1),ones(1,sP(1)),'bx','markersize',10,'linewidth',3);
        h(2) = plot(Q(:,1),ones(1,sQ(1))+ofst,'ro','markersize',8,'linewidth',2.5);
        % draw all minimum distances from set P
        Xp = [P(1:sP(1)) Q(ixp)];
        Yp = [ones(sP(1),1) ones(sP(1),1)+ofst];
        plot(Xp',Yp','-b');
        % draw all minimum distances from set Q
        Xq = [P(ixq) Q(1:sQ(1))];
        Yq = [ones(sQ(1),1) ones(sQ(1),1)+ofst];
        plot(Xq',Yq','-r');
        
        % denote the hausdorff distance
        h(3) = plot(xyf(:,1),[1+ofst;1],'-ks','markersize',12,'linewidth',2);
        uistack(fliplr(h),'top')
        xlabel('Dim 1'); ylabel('visualization offset');
        set(gca,'ytick',[])
        title(['Hausdorff Distance = ' num2str(m)])
        legend(h,{'P','Q','Hausdorff Dist'},'location','best')
        
    elseif sP(2) == 3
        h(1) = plot3(P(:,1),P(:,2),P(:,3),'bx','markersize',10,'linewidth',3);
        h(2) = plot3(Q(:,1),Q(:,2),Q(:,3),'ro','markersize',8,'linewidth',2.5);
        % draw all minimum distances from set P
        Xp = [P(1:sP(1),1) Q(ixp,1)];
        Yp = [P(1:sP(1),2) Q(ixp,2)];
        Zp = [P(1:sP(1),3) Q(ixp,3)];
        plot3(Xp',Yp',Zp','-b');
        % draw all minimum distances from set Q
        Xq = [P(ixq,1) Q(1:sQ(1),1)];
        Yq = [P(ixq,2) Q(1:sQ(1),2)];
        Zq = [P(ixq,3) Q(1:sQ(1),3)];
        plot3(Xq',Yq',Zq','-r');
        
        % denote the hausdorff distance
        h(3) = plot3(xyf(:,1),xyf(:,2),xyf(:,3),'-ks','markersize',12,'linewidth',2);
        uistack(fliplr(h),'top')
        xlabel('Dim 1'); ylabel('Dim 2'); zlabel('Dim 3');
        title(['Hausdorff Distance = ' num2str(m)])
        legend(h,{'P','Q','Hausdorff Dist'},'location','best')
        

    end
    
    subplot(1,2,2)
    % add offset because pcolor ignores final rows and columns
    [X Y] = meshgrid(1:(sQ(1)+1),1:(sP(1)+1));
    hpc = pcolor(X-0.5,Y-0.5,[[D; D(end,:)] [D(:,end); 0]]);
    set(hpc,'edgealpha',0.25)
    xlabel('ordered points in Q (o)')
    ylabel('ordered points in P (x)')
    title({'Distance (color) between points in P and Q';...
        'Hausdorff distance outlined in white'})
    colorbar('location','SouthOutside')
    
    hold on
    % bug: does not draw when hd is the very last point
    rectangle('position',[ixhd(1)-0.5 ixhd(2)-0.5 1 1],...
        'edgecolor','w','linewidth',2);
    
end
end

function [ mhd ] = ModHausdorffDist( A, B )

% Code Written by B S SasiKanth, Indian Institute of Technology Guwahati.
% Website: www.bsasikanth.com
% E-Mail:  bsasikanth@gmail.com
% 
% This function computes the Modified Hausdorff Distance (MHD) which is 
% proven to function better than the directed HD as per Dubuisson et al. 
% in the following work:
% 
% M. P. Dubuisson and A. K. Jain. A Modified Hausdorff distance for object 
% matching. In ICPR94, pages A:566-568, Jerusalem, Israel, 1994.
% http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=576361
% 
% The function computed the forward and reverse distances and outputs the 
% minimum of both.
% 
% Format for calling function:
% 
% MHD = ModHausdorffDist(A,B);
% 
% where
% MHD = Modified Hausdorff Distance.
% A -> Point set 1
% B -> Point set 2
% 
% No. of samples of each point set may be different but the dimension of
% the points must be the same.

% BEGINNING OF CODE

% Compute the sizes of the input point sets
Asize = size(A);
Bsize = size(B);

% Check if the points have the same dimensions
if Asize(2) ~= Bsize(2)
    error('The dimensions of points in the two sets are not equal');
end

% Calculating the forward HD

fhd = 0;                    % Initialize forward distance to 0
for a = 1:Asize(1)          % Travel the set A to find avg of d(A,B)
    mindist = Inf;          % Initialize minimum distance to Inf
    for b = 1:Bsize(1)      % Travel set B to find the min(d(a,B))
        tempdist = norm(A(a,:)-B(b,:));
        if tempdist < mindist
            mindist = tempdist;
        end
    end
    fhd = fhd + mindist;    % Sum the forward distances
end
fhd = fhd/Asize(1);         % Divide by the total no to get average

% Calculating the reverse HD

rhd = 0;                    % Initialize reverse distance to 0
for b = 1:Bsize(1)          % Travel the set B to find avg of d(B,A)
    mindist = Inf;          % Initialize minimum distance to Inf
    for a = 1:Asize(1)      % Travel set A to find the min(d(b,A))
        tempdist = norm(A(a,:)-B(b,:));
        if tempdist < mindist
            mindist = tempdist;
        end
    end
    rhd = rhd + mindist;    % Sum the reverse distances
end
rhd = rhd/Bsize(1);         % Divide by the total no. to get average

mhd = max(fhd,rhd);         % Find the minimum of fhd/rhd as 
                            % the mod hausdorff dist


end









